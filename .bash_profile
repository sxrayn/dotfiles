#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc


export PATH="${PATH}:/home/red/scripts"
export BROWSER="qutebrowser"
export FILE="ranger"
export EDITOR="vim"

wal -Rn
