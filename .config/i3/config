################################################################################
#
#
#       _/  _/_/_/                                          _/_/   
#                _/        _/_/_/    _/_/    _/_/_/      _/        
#     _/    _/_/        _/        _/    _/  _/    _/  _/_/_/_/     
#    _/        _/      _/        _/    _/  _/    _/    _/          
#   _/  _/_/_/          _/_/_/    _/_/    _/    _/    _/           
#
#   
#
#   i3 GAPS CONFIG FILE
#   PYWAL EDITION
#
#   AUTHOR: R. Bormann
#   https://bitbucket.org/sxrayn/dotfiles/src/master/
#
#
################################################################################

font pango:Misc fixed 9

floating_modifier $mod


# VARIABLES
set $mod Mod4

set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

set $monitor1 "DP-2"
set $monitor2 "DP-4"

workspace $ws1 output $monitor1
workspace $ws2 output $monitor1
workspace $ws3 output $monitor1
workspace $ws4 output $monitor1
workspace $ws5 output $monitor1
workspace $ws6 output $monitor1
workspace $ws7 output $monitor1
workspace $ws8 output $monitor1
workspace $ws9 output $monitor1
workspace $ws10 output $monitor2


# KEYBINDINGS
# terminal
bindsym $mod+Return exec i3-sensible-terminal
bindsym $mod+Shift+Return exec urxvt -cd "`xcwd`"

# close
bindsym $mod+Shift+q kill
# rofi
bindsym $mod+d exec rofi -combi-modi "drun#run#ssh" -show combi 
# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"
# lock screen
bindsym $mod+shift+x exec i3lock-fancy-rapid 5 10

# media keys
bindsym XF86AudioRaiseVolume exec --no-startup-id "amixer -q sset 'Master' 5%+"
bindsym XF86AudioLowerVolume exec --no-startup-id "amixer -q sset 'Master' 5%-"
bindsym XF86AudioMute exec --no-startup-id "amixer -q sset 'Master' toggle"

bindsym XF86AudioPlay exec playerctl play-pause
bindsym XF86AudioNext exec playerctl next
bindsym XF86AudioPrev exec playerctl previous

# screenshots
bindsym Print exec --no-startup-id spectacle -mbno Pictures/screenshot_"$(date '+%Y%m%d_%H%M%S').png"               # current monitor
bindsym ctrl+Print exec --no-startup-id spectacle -rbno Pictures/screenshot_"$(date '+%Y%m%d_%H%M%S').png"          # region
bindsym Shift+Print exec --no-startup-id spectacle -abno Pictures/screenshot_"$(date '+%Y%m%d_%H%M%S').png"         # current window
bindsym ctrl+Shift+Print exec --no-startup-id spectacle -fbno Pictures/screenshot_"$(date '+%Y%m%d_%H%M%S').png"    # all monitors

# gaps
bindsym Insert          gaps inner current plus 5
bindsym shift+Insert    gaps inner current minus 5
bindsym ctrl+Insert     gaps inner current set 10

#bindsym Delete          gaps outer current plus 5
#bindsym shift+Delete    gaps outer current minus 5
#bindsym ctrl+Delete     gaps outer current set 0

#bindsym F9              gaps outer current set 100
#bindsym F10              gaps outer current set 50
#bindsym F11              gaps outer current set 0

# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+Shift+n split h

# split in vertical orientation
bindsym $mod+Shift+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10

# resize window 
mode "resize" {
        # vim keys
        bindsym h resize shrink width 10 px or 10 ppt
        bindsym l resize grow width 10 px or 10 ppt

        bindsym j resize shrink height 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt

        # arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt
        
        bindsym Down resize shrink height 10 px or 10 ppt
        bindsym Up resize grow height 10 px or 10 ppt

        # back to normal: Enter or Escape or $mod+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"

# move to scratchpad
bindsym $mod+minus move scratchpad
bindsym $mod+shift+minus scratchpad show


# EXECS
#exec --no-startup-id nm-applet
#exec --no-startup-id caffeine
#exec --no-startup-id blueberry-tray
exec --no-startup-id dunst
exec --no-startup-id xrdb ~/.Xresources
exec --no-startup-id wal -Rn

exec_always --no-startup-id $HOME/.config/polybar/launch.sh
exec_always --no-startup-id compton -f --config ~/.config/compton.conf -b

exec_always --no-startup-id setwp --renew
#exec_always --no-startup-id feh --bg-fill ~/.wallpaper
#exec_always --no-startup-id feh --bg-fill --no-xinerama ~/.wallpaper

exec_always --no-startup-id xinput --set-prop 'Logitech Gaming Mouse G502' 'libinput Accel Profile Enabled' 0, 1
exec_always --no-startup-id xinput --set-prop 'Logitech Gaming Mouse G502' 'libinput Accel Speed' -0.5


# GAPS CONF
gaps inner 10
gaps outer 0

hide_edge_borders both
smart_borders on
for_window [class="^.*"] border pixel 2


# COLORS 
#set_from_resource $primary          i3wm.color4
set_from_resource $primary          i3wm.color4
set_from_resource $complementary    i3wm.color6


set $gray0              #111111
set $gray1              #222222
set $gray2              #333333
set $gray3              #444444

# window colors
#                       border              background         text                 indicator
client.focused          $primary            $primary           $gray0               $complementary
client.unfocused        $gray0              $gray0             $specialgray         $gray0
client.focused_inactive $gray2              $gray2             $specialgray         $gray2
