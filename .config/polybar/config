################################################################################
;
;
;                             _/            _/                            
;        _/_/_/      _/_/    _/  _/    _/  _/_/_/      _/_/_/  _/  _/_/   
;       _/    _/  _/    _/  _/  _/    _/  _/    _/  _/    _/  _/_/        
;      _/    _/  _/    _/  _/  _/    _/  _/    _/  _/    _/  _/           
;     _/_/_/      _/_/    _/    _/_/_/  _/_/_/      _/_/_/  _/            
;    _/                            _/                                     
;   _/                        _/_/                  
;
;   
;
;   POLYBAR CONFIG FILE
;   PYWAL EDITION
;
;   AUTHOR: R. Bormann
;   https://bitbucket.org/sxrayn/archnb-dotfiles/src/master/
;
;
################################################################################

[colors]

gray0 = #99111111
gray1 = #99222222
gray2 = #99333333
gray3 = #99444444
gray4 = #99555555
gray5 = #FF555555

black =         ${xrdb:color0}
red =           ${xrdb:color1}
green =         ${xrdb:color2}
yellow =        ${xrdb:color3}
blue =          ${xrdb:color4}
magenta =       ${xrdb:color5}
cyan =          ${xrdb:color6}
white =         ${xrdb:color7}

fg      = #DDDDDD
fg-alt  = #888888
fg-good = #AEFFAE
fg-bad  = #994C4C
fg-warn = #FF7B00

foreground =        ${colors.blue}
foreground-alt =    ${colors.gray5}
foreground-good =   ${colors.cyan}
foreground-bad =    ${colors.red}
foreground-warn =   $colors.white}

;background =        ${colors.gray1}
background =        #EE111111
background-alt =    ${colors.gray4}



[bar/prime]
monitor = ${env:MONITOR:DP-2}
width = 100%
height = 20
radius = 0.0
fixed-center = false

background = ${colors.background}
foreground = ${colors.foreground}
line-size = 1
line-color = #f00

border-size = 0
border-bottom-size= 0
border-top-size= 0
border-color = #00000000

padding-left = 0
padding-right = 2

module-margin-left = 0
module-margin-right = 2

font-0 = misc fixed:pixelsize=14;1
font-1 = unifont:fontformat=truetype:size=8:antialias=false;0
font-2 = siji:pixelsize=14;1

modules-left = i3 xwindow
modules-center = 
modules-right = spotify pulseaudio xkeyboard temperature caffeine bluetooth nordvpn eth date 

tray-position = right
tray-padding = 5

cursor-click = pointer
cursor-scroll = ns-resize


; MODULES
[module/bluetooth]
type = custom/script
exec = $HOME/.config/polybar/modules/bluetooth.py --daemon
tail = true

click-left = $HOME/.config/polybar/modules/bluetooth.py --toggle
click-right = $HOME/.config/polybar/modules/bluetooth.py --connect C8:84:47:11:07:BA

format-prefix = ""
format-prefix-foreground = ${colors.foreground-alt}
format-underline = ${colors.foreground}

[module/caffeine]
type = custom/script
exec = $HOME/.config/polybar/modules/caffeine.py --daemon
tail = true

click-left = $HOME/.config/polybar/modules/caffeine.py --toggle


[module/kraken]
type = custom/script
tail = true
exec = python3 $HOME/.config/polybar/modules/kraken.py

format = <label>
label = %output%


[module/nordvpn]
type = custom/script
exec = $HOME/.config/polybar/modules/nordvpn.py
tail = true

click-left = $HOME/.config/polybar/modules/nordvpn.py --toggle de
format = <label>
label = %output%

format-prefix = ""
format-prefix-foreground = ${colors.foreground-alt}
format-prefix-underline = ${colors.foreground}


[module/crypto]
type = custom/script
exec = python3 $HOME/.config/polybar/modules/crypto_rates.py

interval = 20
format = <label>
label = %output%


[module/xwindow]
type = internal/xwindow
label = %title%
label-maxlen = 120


[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock

format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-prefix-underline = ${colors.foreground}

label-layout = %layout%
label-layout-underline = ${colors.foreground}

label-indicator-padding = 2
label-indicator-margin = 1
label-indicator-background = ${colors.foreground-warn}
label-indicator-underline = ${colors.foreground-warn}


[module/filesystem]
type = internal/fs
interval = 25

mount-0 = /

label-mounted = %{F#0a81f5}%mountpoint%%{F-}: %percentage_used%%
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.foreground-alt}

[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = false

; Only show workspaces on the same output as the bar
;pin-workspaces = true

label-mode-padding = 2
label-mode-foreground = #000
label-mode-background = ${colors.foreground}

; focused = Active workspace on focused monitor
label-focused = %index%
label-focused-background = ${colors.background-alt}
label-focused-underline= ${colors.foreground}
label-focused-padding = 2

; unfocused = Inactive workspace on any monitor
label-unfocused = %index%
label-unfocused-padding = 2

; visible = Active workspace on unfocused monitor
label-visible = %index%
label-visible-background = ${self.label-focused-background}
label-visible-underline = ${self.label-focused-underline}
label-visible-padding = ${self.label-focused-padding}

; urgent = Workspace with urgency hint set
label-urgent = %index%
label-urgent-background = ${colors.foreground-bad}
label-urgent-padding = 2

; Separator in between workspaces
; label-separator = |


[module/spotify]
type = custom/script
interval = 1
format-prefix = " "
format = <label>
exec = python $HOME/.config/polybar/modules/spotify_status.py -f '{artist}: {song}'
format-underline = #1db954


[module/mpd]
type = internal/mpd
format-online = <label-song>  <icon-prev> <icon-stop> <toggle> <icon-next>

icon-prev = 
icon-stop = 
icon-play = 
icon-pause = 
icon-next = 

label-song-maxlen = 25
label-song-ellipsis = true


[module/cpu]
type = internal/cpu
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-underline = #f90000
label = %percentage:2%%


[module/memory]
type = internal/memory
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-underline = #4bffdc
label = %percentage_used%%


[module/eth]
type = internal/network
interface = enp12s0
interval = 3.0

; connected
label-connected = " %linkspeed%"
format-connected-prefix = ""
format-connected-prefix-foreground = ${colors.foreground-alt}
format-connected-prefix-underline = ${colors.foreground}

format-connected-underline = ${colors.foreground-good}

; disconnected 
label-disconnected = " %ifname% disconnected"
label-disconnected-foreground = ${colors.foreground-alt}
format-disconnected-prefix = ""
format-disconnected-prefix-foreground = ${colors.foreground-alt}
format-disconnected-prefix-underline = ${colors.foreground}

format-disconnected = <label-disconnected>
format-disconnected-underline = ${colors.foreground-warn}


[module/date]
type = internal/date
interval = 5

date = "%Y-%m-%d"
date-alt = " %Y-%m-%d"

time = %H:%M
time-alt = %H:%M:%S

;format-prefix = 
format-prefix-foreground = ${colors.foreground-alt}
format-underline = ${colors.foreground}

label = %date% %time%


[module/pulseaudio]
type = internal/pulseaudio

format-volume = <ramp-volume> <label-volume>
format-volume-underline = ${colors.foreground}

ramp-volume-0 = 
ramp-volume-1 = 
ramp-volume-2 = 
ramp-volume-foreground = ${colors.foreground-alt}

format-muted-prefix = " "
format-muted-foreground = ${colors.foreground-alt}
label-muted = mute 


[module/temperature]
type = internal/temperature
thermal-zone = 0
hwmon-path = /sys/class/hwmon/hwmon1/temp1_input
warn-temperature = 60

format = <ramp> <label>
format-underline = ${colors.foreground-good}

format-warn = <ramp> <label-warn>
format-warn-underline = ${colors.foreground-bad}

label = %temperature-c%
label-warn = %temperature-c%
label-warn-foreground = ${colors.foreground-bad}

ramp-0 = 
ramp-1 = 
ramp-2 = 
ramp-foreground = ${colors.foreground-alt}

[module/powermenu]
type = custom/menu

expand-right = true
format-spacing = 1
format-underline = ${colors.foreground-bad}

label-open = 
label-open-foreground = ${colors.foreground-bad}
label-close =  cancel
label-close-foreground = ${colors.foreground-bad}
label-separator = |
label-separator-foreground = ${colors.foreground-alt}

menu-0-0 = reboot
menu-0-0-exec = menu-open-1
menu-0-1 = power off
menu-0-1-exec = menu-open-2

menu-1-0 = cancel
menu-1-0-exec = menu-open-0
menu-1-1 = reboot
menu-1-1-exec = systemctl reboot

menu-2-0 = power off
menu-2-0-exec = systemctl poweroff
menu-2-1 = cancel
menu-2-1-exec = menu-open-0


[settings]
screenchange-reload = true


[global/wm]
margin-top = 5
margin-bottom = 5


; vim:ft=dosini
