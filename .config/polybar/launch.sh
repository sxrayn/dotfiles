#!/usr/bin/env bash

killall -q polybar

while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done
~/.config/polybar/modules/extract_colors.py
polybar prime &
echo "success"
