#!/usr/bin/env python3

import sys
import subprocess
from time import sleep
from extract_colors import load_colors
from helper import printf, strstdout


def status():
    status = subprocess.run(["pgrep", "-f", "caffeine-ng"], stdout=subprocess.PIPE)
    s = strstdout(status.stdout)
    s = s[:-1]
    return not s == ''


def daemon():
    colors = load_colors()
    pre = '%{F' + colors['foreground-alt'] + '}%{u' + colors['foreground'] + '}'
    f_run = '%{u' + colors['foreground-good'] + '}%{F' + colors['foreground-good'] + '}'
    f_norun = '%{u' + colors['foreground-bad'] + '}%{F' + colors['foreground-alt'] + '}'
    while True:
        if status():
            printf(pre + '' + f_run + ' caf')
        else:
            printf(pre + '' + f_norun + ' off')
        sleep(1)


def toggle():
    if status():
        subprocess.run(["caffeine", "kill"])
        subprocess.run(["notify-send", "caffeine", "disabled caffeine",  "--urgency=low", "--expire-time=1000"])
    else:
        subprocess.Popen(["caffeine", "-a"])
        subprocess.run(["notify-send", "caffeine", "enabled caffeine", "--urgency=low", "--expire-time=1000"])


def main():
    if len(sys.argv) > 1:
        if sys.argv[1] == '--daemon':
            daemon()
        elif sys.argv[1] == '--toggle':
            toggle()
    else:
        daemon()

if __name__ == "__main__":
    main()
