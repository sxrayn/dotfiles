#!/usr/bin/env python3


import krakenex
import sys
from time import sleep


SEPERATOR = ' | '
k = krakenex.API()
k.load_key('/home/red/.config/polybar/modules/kraken.key')


def get_balance():
    """Request current balance from kraken api."""
    return k.query_private('Balance')['result']


def get_all_valid_pairs():
    """Request all tradeable currency pairs from kraken api."""
    return list(k.query_public('AssetPairs')['result'].keys())


def get_pairs(balance, valid):
    """Build all valid <fiat, crypto> pairs from owned assets."""
    assets = list(balance.keys())
    fiat = assets[0]
    # Compensate for kraken naming inconsistency
    pairs = [a + fiat for a in assets[1:]
            if a + fiat in valid]
    pairs2 = [a + fiat[1:] for a in assets[1:]
            if a + fiat[1:] in valid]
    return pairs + pairs2


def get_exchange_rates(pairs):
    """Request current exchange rates from kraken api."""
    # Build comma seperated pair list
    pstr = 'pair='
    for p in pairs:
        pstr += p + ','
    pstr = pstr[:-1] # remove last comma
    return k.query_public('Ticker', data=pstr)['result']


def get_currency_exchange_relation(balance, pairs):
    """Build currency exchange relation."""
    return {k: p for k in balance.keys()
            for p in pairs if k in p}


def get_asset_value(balance, pairs, rates):
    """Calculates current asset value."""
    cexr = get_currency_exchange_relation(balance, pairs)
    return {k: (float(balance[k]) * float(rates[v]['b'][0]))
            for k, v in cexr.items()}


def printf(string, **kwargs):
    """Assure printing without terminal."""
    print(string, **kwargs)
    sys.stdout.flush()


def human_readable_pairs(pair: str) -> str:
    if pair[4] == 'Z':
        pair = pair[:4] + pair[5:]
    if pair[0] == 'X':
        pair = pair[1:]
    return pair

def ticker(dt=2, pull_api=200, print_rates=False, max_str_length=0):
    balance = get_balance()
    valid = get_all_valid_pairs()
    pairs = get_pairs(balance, valid)
    rates = get_exchange_rates(pairs)
    aval = get_asset_value(balance, pairs, rates)

    i = 0
    offset = 0
    while True:
        if i >= pull_api:
            balance = get_balance()
            pairs = get_pairs(balance, valid)
            rates = get_exchange_rates(pairs)
            aval = get_asset_value(balance, pairs, rates)
            i = 0
        output = "VALUE: " + str(round(sum(aval.values()), 2))
        if print_rates:
            output += SEPERATOR
            for k, v in rates.items():
                kr = human_readable_pairs(k)
                output += kr + ': ' + str(round(float(v['b'][0]), 2)) + SEPERATOR
        if max_str_length > 0:
            if offset > len(output):
                offset = 0
            printf((output[offset:] + output[:offset])[:max_str_length])
            offset += 1
        else:
            printf(output[:-3])
        sleep(dt)
        i += dt


if __name__ == "__main__":
    ticker(dt=1, pull_api=400, print_rates=True, max_str_length=75)
