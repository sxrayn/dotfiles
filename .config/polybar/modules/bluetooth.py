#!/usr/bin/env python3

import sys
import subprocess

from helper import printf, strstdout
from time import sleep
from extract_colors import load_colors

def status():
    st = subprocess.run(["bluetoothctl", "show"], stdout=subprocess.PIPE)
    s = ""
    for c in st.stdout:
        s += chr(c)
    s = s.split('\n')
    pwr = s[4].split()
    if pwr[1] == 'yes':
        return True
    else:
        return False


def device():
    st = subprocess.run(["bluetoothctl", "info"], stdout=subprocess.PIPE)
    s = strstdout(st.stdout)
    if s.split()[0] == 'Missing':
        return [False, 'no dev']
    else:
        return [True, s.split('\n')[1].split()[1]]


def daemon():
    colors = load_colors()
    f_con= '%{u' + colors['foreground-good'] + '}'
    f_dis = '%{u' + colors['foreground-bad'] + '}%{F' + colors['foreground-alt'] + '}'

    while True:
        if status():
            d = device()
            if d[0]:
                printf(f_con + ' ' + d[1])
            else:
                printf(f_dis + ' ' + d[1])
        else:
            printf(f_dis + ' off')
        sleep(1)


def toggle():
    cmd = 'on'
    if status():
        cmd = 'off'
    subprocess.run(["bluetoothctl", "power", cmd])


def connect():
    if status() and len(sys.argv) > 2:
        cmd = "connect"
        if device()[0]:
            cmd = "disconnect"
        subprocess.run(["bluetoothctl", cmd, sys.argv[2]])


def main():
    argd = {'--daemon': daemon,
            '--toggle': toggle,
            '--connect': connect}
    if len(sys.argv) > 1:
        argd.get(sys.argv[1], daemon)()
    else:
        daemon()


if __name__ == "__main__":
    main()
