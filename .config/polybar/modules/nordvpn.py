#!/usr/bin/env python3

import sys
import subprocess
from time import sleep
from extract_colors import load_colors
from helper import printf, strstdout


def status():
    status = subprocess.run(["nordvpn", "status"], stdout=subprocess.PIPE)
    s = strstdout(status.stdout)
    s = s.split('\n')

    if s[0].split()[2] == 'Connected':
        return [True, s[1].split()[2]]
    else:
        return [False, 'disconnected']


def daemon():
    colors = load_colors()
    f_con= '%{u' + colors['foreground-good'] + '}'
    f_dis = '%{u' + colors['foreground-bad'] + '}%{F' + colors['foreground-alt'] + '}'

    while True:
        s = status()
        if s[0]:
            printf(f_con + ' ' + s[1])
        else:
            printf(f_dis + ' ' + s[1])
        sleep(1)


def toggle():
    command = ["nordvpn"]

    if status()[0]:
        command += ["d"]
    else:
        command += ["c"]
        if len(sys.argv) > 2:
            command += [sys.argv[2]]
    subprocess.run(command)


def main():
    if len(sys.argv) > 1:
        if sys.argv[1] == '--daemon':
            daemon()
        elif sys.argv[1] == '--toggle':
            toggle()
    else:
        daemon()

if __name__ == "__main__":
    main()
