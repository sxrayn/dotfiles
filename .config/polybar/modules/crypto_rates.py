#!/usr/bin/env python3


import requests
import sys
from time import sleep


API_URL = 'https://api.bitfinex.com/v2/ticker/'
API_KEY = ''

SPACE = "  "
SEPERATOR = SPACE + "|" + SPACE

DEFAULT_REFRESH_RATE = 200
DEFAULT_DELTA = 1
DEFAULT_LENGTH = -1

ticker_mode = False


def ticker(api, currencies, length, delta, refresh) -> str:
    i = 0
    offset = 0
    rates = get_rates(api, currencies)

    while True:
        # request rates
        if i >= refresh:
            rates = get_rates(api, currencies)
            i = 0

        # build string
        string = build_string(rates)
        if offset > len(string):
            offset = 0
        string = string[offset:] + string[:offset]

        # display whole string if length was not set
        if length == DEFAULT_LENGTH:
            print_flush(string)
        else:
            print_flush(string[:length])

        sleep(delta)
        i += delta
        offset += 1


def print_flush(string, **kwargs):
    """Assure printing without terminal"""
    print(string, **kwargs)
    sys.stdout.flush()


def build_string(rates) -> str:
    global ticker_mode
    string = ""
    for rate in rates:
        string += rate[0] + SPACE + rate[1] + SEPERATOR
    if ticker_mode:
        return string
    else:
        return string[:-len(SEPERATOR)]


def get_rates(api: str, currencies: dict) -> list:
    rates = []
    for key, value in currencies.items():
        rate = requests.get(api + value).text[1:].split(',')[0]
        rates.append([value[1:], rate])
    return rates


def main():
    global ticker_mode
    currencies = {
        "btc": "tBTCUSD",
        "eth": "tETHUSD",
        "xrp": "tXRPUSD"}

    # ticker mode
    if "--ticker" in sys.argv:
        ticker_mode = True
        delta = DEFAULT_DELTA
        length = DEFAULT_LENGTH
        refresh = DEFAULT_REFRESH_RATE

        for i in range(len(sys.argv)):
            # set delay between updates
            if sys.argv[i] == "-d" and len(sys.argv) > i:
                delta = float(sys.argv[i+1])
            # set max string length
            elif sys.argv[i] == "-l" and len(sys.argv) > i:
                length = int(sys.argv[i+1])
            # set refresh rate
            elif sys.argv[i] == "-r" and len(sys.argv) > i:
                refresh = int(sys.argv[i+1])
        print(length, delta, refresh)
        ticker(API_URL, currencies, length, delta, refresh)

    # static mode
    else:
        print(build_string(get_rates(API_URL, currencies)))


if __name__ == "__main__":
    main()
