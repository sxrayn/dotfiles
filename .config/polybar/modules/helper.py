#!/usr/bin/env python3


import sys


def printf(s, **kwargs):
    print(s, **kwargs)
    sys.stdout.flush()


def strstdout(stdout):
    s = ""
    for c in stdout:
        s += chr(c)
    return s
