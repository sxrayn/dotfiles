#!/usr/bin/env python3

import pickle
from pathlib import Path

home = str(Path.home())
conff = home + "/.config/polybar/config"
colorf = home + "/.config/polybar/colors.pkl"
walf = home + "/.cache/wal/colors"

def get_colors():
    xf = {}
    xrdb = {'black': 0,
            'red': 1,
            'green': 2,
            'yellow': 3,
            'blue': 4,
            'magenta': 5,
            'cyan': 6,
            'white': 7}

    # get foregrounds from i3 config
    foregrounds = []
    with open(conff) as f:
        for l in f:
            if "foreground" in l:
                foregrounds += [l]

    # get xrdb color name
    foregrounds = foregrounds[:5]
    fgs = {}
    for fg in foregrounds:
        n = fg.split()[2].split('.')[1][:-1]
        if n in xrdb.keys():
            fgs[fg.split()[0]] = n   
        else:
            with open(conff) as f:
                for l in f:
                    if n in l:
                        xf[fg.split()[0]] = l.split('=')[1][1:-1]
                        break


    # get xrdb hex value
    xhex = []
    with open(walf) as f:
        for l in f:
            xhex += [l]

    # build foreground name: xrdb hex code dict
    for k, v in fgs.items():
        xf[k] = xhex[xrdb[v]][:-1]
    return xf


def save_colors(d):
    with open(colorf, 'wb') as f:
        pickle.dump(d, f, pickle.HIGHEST_PROTOCOL)


def load_colors():
    with open(colorf, 'rb') as f:
        return pickle.load(f)


if __name__ == "__main__":
    save_colors(get_colors())
